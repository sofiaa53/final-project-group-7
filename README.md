
# Final Project
Kelompok 7
---------------
### Anggota Kelompok
* Aulia Anugrah Aziz
* Dedi Triyadi Barnawan
* Sofia Awiliyah

### Tema Project
Tema Project merupakan situs E-Commerce yang berjudul JualBeli. Terdapat 3 fitur CRUD, yakni untuk product, category, dan transaksi. Situs ini menggunakan laravel sweet alert untuk pop-up, TinyMCE untuk mengedit deskripsi product, dan Datatables untuk menampilkan list category.
### ERD
![ERD Kelompok 7](/public/image/erd.png "erd")
### Link Video
Link Demo Aplikasi  : https://youtu.be/Ewfpv3DSOKM


### Inisiasi awal

* 1 copy file .env.example ke file .env dan edit db_name dengan yang ada di local

* composer install

* php artisan key:generate

* php artisan migrate

* npm install

* npm run dev

* php artisan serve

http://localhost:8000/admin

* username : admin@mail.com

* password : 1122334455