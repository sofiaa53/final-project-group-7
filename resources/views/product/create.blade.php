@extends('layouts.dashboard')

@section('judul')
    Tambah Produk
@endsection

@section('content')
    <form action="/product" method="POST" enctype='multipart/form-data'>
        @csrf

        <div class="form-group">
            <label>Kategori</label>
            <select name="category_id" class="form-control" id="">
                <option value="">--pilih kategori--</option>
                @forelse ($category as $item)
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                @empty
                    <option value="">Tidak ada data</option>
                @endforelse
            </select>
        </div>
        @error('category_id')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Nama </label>
            <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Detail </label>
            <textarea name="detail" class="form-control" id="myeditorinstance" cols="30" rows="10"></textarea>
        </div>
        @error('detail')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Harga Satuan</label>
            <input type="number" name="harga" class="form-control">
        </div>
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
            <label>Image</label>
            <input type="file" name="gambar" class="form-control">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@push('scripts')
    <script src="https://cdn.tiny.cloud/1/6h73pix2ztb30ys1zs1dxnou8xiew9akgbyvd9vrl3qo6gh3/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea#myeditorinstance', // Replace this CSS selector to match the placeholder element for TinyMCE
            plugins: 'code table lists',
            toolbar: 'undo redo | blocks | bold italic | alignleft aligncenter alignright | indent outdent | bullist numlist | code | table'
        });
    </script>
@endpush
@endsection
    
    