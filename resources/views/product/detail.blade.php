@extends('layouts.dashboard')

@section('judul')
    Halaman Detail Produk
@endsection

@section('content')
    <img src="{{ asset('image/' . $product->gambar) }}" class="rounded mx-auto d-block my-3" alt="...">
    <div class="card-body">
        <h3 class="">{{ $product->nama }}</h3>
        <p class="card-text">{{ $product->detail }}</p>
        <a href="/product" class="btn btn-secondary btn-block btn-sm">Kembali</a>
    </div>

    <br>

    <h3>List Komentar</h3>
    @forelse ($product->comment as $item)
        <div class="card">
            <div class="card-header bg-dark">
                {{$item->user->name}} <br> {{$item->created_at}}
            </div>
            <div class="card-body">
                <p class="card-text">{{$item->content}}</p>
            </div>
        </div>
    @empty
        <div class="alert alert-danger">Belum ada komentar</div>
    @endforelse
    <form action="/comment/{{ $product->id }}" method="POST">
        @csrf
        <div class="form-group">
            <textarea name="content" class="form-control @error('content') is-invalid @enderror" id="" cols="30"
                rows="10" placeholder="Buat komentar..."></textarea>
        </div>
        @error('content')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Tambah Komentar</button>
    </form>
@endsection