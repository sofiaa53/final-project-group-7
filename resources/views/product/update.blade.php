@extends('layouts.dashboard')

@section('judul')
Halaman Tambah product
@endsection

@section('content')
<form action="/product/{{$product->id}}" method="POST" enctype='multipart/form-data'>
    @csrf
    @method('PUT')

    <div class="form-group">
      <label>Kategori</label>
      <select name="category_id" class="form-control" id="" value="{{$product->category_id}}">
          <option value="">--pilih kategori--</option>
          @forelse ($category as $item)
              @if($item->id === $product->category_id)
                  <option value="{{$item->id}}"selected>{{$item->nama}}</option>
              @else
              <option value="{{$item->id}}">{{$item->nama}}</option>
              @endif
              
          @empty
              <option value="">Tidak ada data</option>
          @endforelse
        </select>        
    </div>
    @error('category_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Nama Produk</label>
      <input type="text" name="nama" value = "{{$product->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Detail Produk</label>
      <textarea name="detail" class="form-control" id="" cols="30" rows="10">{{$product->detail}}</textarea>
    </div>
    @error('detail')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Harga</label>
        <input type="number" name="harga" class="form-control" value="{{$product->harga}}">
    </div>
    @error('harga')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Gambar</label>
      <input type="file" name="gambar" class="form-control">
    </div>
    @error('gambar')
      <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
