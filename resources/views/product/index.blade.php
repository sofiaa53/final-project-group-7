@extends('layouts.dashboard')

@section('judul')
Halaman List Produk
@endsection

@section('content')
<a href="/product/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<div class="row">
    @forelse ($product as $item)
        <div class="col-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('image/' . $item->gambar)}}" class="card-img-top" alt="...">
                <div class="card-body">
                  <h3 class="">{{$item->nama}}</h3>
                  <p class="card-text">{{Str::limit($item->ringkasan, 30)}}</p>
                  <a href="/product/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Klik Detail</a>
                  <div class="row my-2">
                    <div class = "col">
                        <a href="/product/{{$item->id}}/edit" class="btn btn-info btn-sm mb-2">Edit Data</a>
                    </div>

                    <div class = "col">
                        <form action ="product/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <input type="submit" class="btn btn-danger btn-block btn-sm" value="Delete">
                        </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>   
    @empty
        <h2>Belum ada produk saat ini</h2>
    @endforelse
</div>

@endsection