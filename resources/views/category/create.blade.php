@extends('layouts.dashboard')

@section('judul')
Halaman Tambah Produk
@endsection

@section('content')
<form action="/category" method="post" enctype='multipart/form-data'>
    @csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
