@extends('layouts.dashboard')

@section('judul')
Halaman Edit Cast
@endsection

@section('content')
<form action="/category/{{$category->id}}" method="post">
    @csrf
    @method("put")
    <div class="form-group">
      <label>Nama kategori</label>
      <input type="text" name="nama" value={{$category->nama}} class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
