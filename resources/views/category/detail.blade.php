@extends('layouts.dashboard')

@section('judul')
Detail Kategory
@endsection

@section('content')
<h3>List {{$category->nama}}</h3>

<div class="row">
  @forelse ($category->product as $item)
      <div class="col-4">
          <div class="card" style="width: 18rem;">
              <img src="{{asset('image/' . $item->gambar)}}" class="card-img-top" alt="...">
              <div class="card-body">
                <h3 class="">{{$item->judul}}</h3>
                <p class="card-text">{{Str::limit($item->ringkasan, 30)}}</p>
                <a href="/product/{{$item->id}}" class="btn btn-secondary btn-block btn-sm">Klik Detail</a>
              </div>
          </div>
      </div>   
  @empty
      <h2>Belum ada produk dalam kategori ini</h2>
  @endforelse
</div>

<a href="/category" class="btn btn-secondary btn-sm">Kembali</a>
@endsection