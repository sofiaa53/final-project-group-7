@extends('layouts.dashboard')

@section('judul')
Halaman List Kategori Produk
@endsection

@section('content')
<a href="/category/create" class="btn btn-primary btn-sm mb-3">Tambah</a>
<table id="category" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Kategori</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($category as $key => $value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->nama}}</td>
                <td>
                    <form action="/category/{{$value->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/category/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/category/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class = "btn btn-danger btn-sm">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection