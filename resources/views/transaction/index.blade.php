@extends('layout.master')

@section('judul')
Halaman List Transaksi
@endsection

@push('scripts')
    <script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(document).ready( function () {
        $("#transaction").DataTable();
    });
    </script>
@endpush

@push('styles')
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<table id="transaction" class="table table-bordered table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">ID Transaksi</th>
        <th scope="col">Nama Pengguna</th>
        <th scope="col">Nama Produk</th>
        <th scope="col">Quantity</th>
        <th scope="col">Tanggal Dibayar</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($transaction as $key => $value)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$value->id}}</td>
                <td>{{$value->profil_id}}</td>
                <td>{{$value->produk_id}}</td>
                <td>{{$value->quantity}}</td>
                <td>{{$value->paid_at}}</td>
            </tr>
        @empty
            <tr>
                <td>Tidak ada data</td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection