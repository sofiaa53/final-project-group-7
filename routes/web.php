<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.home', ['category' => \App\Models\Category::get(), 'product' => \App\Models\Product::get()]);
});

Route::get('/shop', function () {
    return view('frontend.index', ['category' => \App\Models\Category::get(), 'product' => \App\Models\Product::get()]);
});

Route::get('/contact', function () {
    return view('frontend.contact', ['category' => \App\Models\Category::get()]);
});

Route::group(['middleware' => ['auth']], function () {
    
    // Route update Profile
    Route::get('/profile', [ProfileController::class, 'index']);
    Route::put('/profile/{id}', [ProfileController::class, 'update']);
    
    // Route Comment
    Route::post('/comment/{product_id}', [CommentController::class, 'store']);
    
});

Route::resource('category', CategoryController::class);
Route::resource('product', ProductController::class);
Route::resource('transaction', TransactionController::class);

Auth::routes();

Route::get('/dashboard', [HomeController::class, 'index']);