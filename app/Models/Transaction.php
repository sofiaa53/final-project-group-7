<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table='transaction';
    protected $fillable = ['profil_id', 'produk_id', 'harga', 'quantity', 'paid_at'];
}
