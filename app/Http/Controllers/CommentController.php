<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Comment;

class CommentController extends Controller
{
    function store(Request $request, $product_id)
    {
        $request->validate([
            'content' => 'required'
        ]);

        $userId = Auth::user()->id;
        $comment = new Comment;

        $comment->user_id = $userId;
        $comment->product_id = $product_id;

        $comment->content = $request->content;

        $comment->save();

        return redirect("/product/". $product_id);
    }
}
