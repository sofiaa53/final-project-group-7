<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        $id = Auth::id();
        $profile = Profile::where('user_id', $id)->first();

        return view('profile.edit', ['profile' => $profile]);

    }

    public function update($id, Request $request)
    {
        $request->validate([
            'alamat' => ['required'],
            'no_telp' => ['required'],
            'umur' => ['required'],
            'jenis_kelamin' => ['required']
        ]);

        $profile = Profile::find($id);

        $profile->alamat = $request['alamat'];
        $profile->no_telp = $request['no_telp'];
        $profile->umur = $request['umur'];
        $profile->jenis_kelamin = $request['jenis_kelamin'];
        if($profile->saldo == 0) {
            $profile->saldo = 0;
        } else {
            $profile->saldo = $request['saldo'];
        }

        $profile->save();

        return redirect('/profile');
    }
}
