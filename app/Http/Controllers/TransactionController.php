<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\User;
use App\Models\Product;
use File;

class TransactionController extends Controller
{
    public function index()
    {
        $transaction = Transaction::get();
        return view('transaction.index', ['transaction' => $transaction]);
    }

    public function create()
    {
         $product = Product::get();
         $user = User::get();
         return view('transaction.create', ['product' => $product, 'user' => $user]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'produk_id' => 'required',
            'harga' => 'required',
            'quantity' => 'required',
            'paid_at' => 'required'
        ]);

        $transactions = new Transaction;
        $transactions->user_id = $request->user_id;
        $transactions->product_id = $request->product_id;
        $transactions->harga = $request->harga;
        $transactions->quantity = $request->quantity;
        $transactions->paid_at = $request->paid_at;;

        $transactions->save();
        return redirect('/transaction');
    }

    public function show($id)
    {
        $transaction = Transaction::find($id);
        return view('transaction.detail', ['transaction'=> $transaction]);
    }

    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $product = Product::get();
        $user = User::get();

        return view('transaction.update', ['transaction' => $transaction, 'product' => $product, 'user'=>$user]);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'user_id' => 'required',
            'produk_id' => 'required',
            'harga' => 'required',
            'quantity' => 'required',
            'paid_at' => 'required'
        ]);

        $transaction = Transaction::find($id);
        $transaction->user_id = $request['user_id'];
        $transaction->product_id = $request['product_id'];
        $transaction->harga = $request['harga'];
        $transaction->quantity = $request['quantity'];
        $transaction->paid_at = $request['paid_at'];

        $transaction->save();
        return redirect('/transaction');

    }

    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        $transaction->delete();
        return redirect('/transaction');
    }

}
