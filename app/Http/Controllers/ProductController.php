<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use File;

class ProductController extends Controller
{
    
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        $product = Product::all();
        return view('product.index', ['product' => $product]);
    }

    public function create()
    {
        $category = Category::get();
        return view('product.create', ['category' => $category]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'detail' => 'required',
            'gambar' => 'required|image|mimes:jpg,png,jpeg',
            'harga' => 'required',
            'category_id' => 'required'
        ]);

        $filename = time().'.'.$request->gambar->extension();
        $request->gambar->move(public_path('image'), $filename);

        $products = new Product;
        $products->nama = $request->nama;
        $products->detail = $request->detail;
        $products->gambar = $filename;
        $products->harga = $request->harga;
        $products->rating = $request->rating;
        $products->category_id = $request->category_id;

        $products->save();

        Alert::success('Success', 'Berhasil menambahkan produk baru!');

        return redirect('/product');
    }

    public function show($id)
    {
        $product = Product::find($id);
        return view('product.detail', ['product'=> $product]);
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $category = Category::get();

        return view('product.update', ['product' => $product, 'category' => $category]);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'detail' => 'required',
            'gambar' => 'mimes:jpg,png,jpeg',
            'harga' => 'required',
            'category_id' => 'required'
        ]);

        $product = Product::find($id);
        if($request->has('gambar')) {
            $path = 'image/';
            File::delete($path. $product->gambar);

            $imageName = time().'.'.$request->gambar->extension();
            $request->gambar->move(public_path('image'), $imageName);

            $product->gambar = $imageName;
        }
        $product->nama = $request['nama'];
        $product->detail = $request['detail'];
        $product->harga = $request['harga'];
        $product->rating = $request['rating'];
        $product->category_id = $request['category_id'];

        $product->save();

        return redirect('/product');

    }

    public function destroy($id)
    {
        $product = Product::find($id);

        $path = 'image/';
        File::delete($path. $product->gambar);

        $product->delete();
        
        return redirect('/product');
    }
}
