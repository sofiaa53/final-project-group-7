<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        $category = Category::get();
        return view('category.index', ['category' => $category]);
    }

    public function create()
    {
         $category = Category::get();
         return view('category.create', ['category' => $category]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $categorys = new Category;
        $categorys->nama = $request->nama;

        $categorys->save();

        return redirect('/category');
    }

    public function show($id)
    {
        $category = Category::find($id);
        return view('category.detail', ['category'=> $category]);
    }

    public function edit($id)
    {
        $category = Category::find($id);
        //$cast = cast::get();

        //return view('category.update', ['category' => $category, 'cast' => $cast]);
        return view('category.update', ['category' => $category]);
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $category = category::find($id);

        $category->nama = $request['nama'];

        $category->save();
        return redirect('/category');

    }

    public function destroy($id)
    {
        $category = Category::find($id);

        $category->delete();
        return redirect('/category');
    }
}
